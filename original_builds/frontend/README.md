
Standalone React app to interact with API

Description:
Initial build of React app.

Requirements: (Based on Ubuntu 20)

npm - 8.12
npm packages:
axios date-fns

npm start


Basic usage: http://localhost:3000
        Actons: Create, Delete, Update
        Update: Will update created_at field with new timstamp
        Order by: List is ordered by Desc. date/time
