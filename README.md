# Fullstack - docker-compose | PostgreSQL | Flask | React |

## Requirements:
Docker + Docker-compose

Ports available and firewall rules to allow access - 5000, 3000, 5432

## Components:

### Backend: Python flask
#### Description: Basic CRUD API using Postgresql Database.
Basic usage: http://localhost:5000

* GET - /events = All items returned
* GET - /events/{id} = Single item returned
* POST - /events = Add single event

	(json - "Description": "INPUT")

* PUT - /events/{id} = Update single item

	(json - "Description": "INPUT")

### Database: Postgresql
#### Description: Basic table with [id, description & created_at]
Basic usage: postgresql://localhost:5432
DB is created when Backend is initialised

### Frontend: React
#### Description: Basic frontend to control API
Basic usage: http://localhost:3000

Actions: Create, Delete, Update

Update: Will update created_at field with new timestamp

Order by: List is ordered by description date/time

***

## Deploy:

Using ./.env file for passing details into the containers

This file needs to be created in the root folder, alongside docker-compose.json (rename .env.template > .env and update variables):

	POSTGRES_USER=postgres (Can be changed..)
	POSTGRES_PASSWORD=[UNIQUE]
	POSTGRES_DB=events (Fixed - Change postgresql Model, python flask if changed)
	POSTGRES_PORT=5432 (Fixed - Change docker-compose port for db if changed)
	POSTGRES_HOST=db (Fixed - Change docker-compose hostname for db if changed)

Run the following to start environment:

	docker-compose up --build

Notes: This solution will only work from your local machine using localhost.  CORS will block access if using IP or hostname to access the website.
